import pickle
import os.path
import simplejson

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


# If modifying these scopes, delete the file directory-token.pickle.
SCOPES = [
    'https://www.googleapis.com/auth/admin.directory.user',
    'https://www.googleapis.com/auth/admin.directory.group',
    'https://www.googleapis.com/auth/admin.directory.group.member',
]


def parse_user_record(record):
    """
    Gets a record from google directory and outputs it in a more convenient format.
    :param record: The google directory record
    :return: A dictionary with just the needed fields.
    """
    out = {}
    out['EMAIL'] = record['primaryEmail']
    out['ID'] = record['id']
    try:
        out['FULLNAME'] = record['name']['fullName']
        out['GIVENNAME'] = record['name']['givenName']
        out['FAMILYNAME'] = record['name']['familyName']
    except KeyError:
        out['FULLNAME'] = None
        out['GIVENNAME'] = None
        out['FAMILYNAME'] = None

    out['ADMIN'] = record['isAdmin']
    out['SUSPENDED'] = record['suspended']
    out['ARCHIVED'] = record['archived']

    try:
        external_ids = record['externalIds']
        for id in external_ids:
            if id['type'] == 'organization':
                out['EDUCATIVE_ADMINISTRATION_CODE'] = id['value']
                break
    except KeyError:
        out['EDUCATIVE_ADMINISTRATION_CODE'] = None

    return out

def parse_group_record(record):
    out = {}
    out['ID'] = record['id']
    out['EMAIL'] = record['email']
    out['NAME'] = record['name']
    out['DESCRIPTION'] = record['description']
    try:
        out['NUM_MEMBERS'] = int(record['directMembersCount'])
    except KeyError:
        out['NUM_MEMBERS'] = 0

    return out

def get_users(service, max_results=500, order_by='email', sort_order='ASCENDING'):
    """
    Returns the list of all the users that are defined in the directory.
    It allows to order by different fields and to limit the number of records
    that we are looking for.
    :param service: Google service reference
    :param max_results: Maximum number of records that we want to retrieve
    :param order_by: Field by we want to sort the output.
    :param sort_order: Order used to sort the group list, can be 'ASCENDING' or 'DESCENDING'

    :return: A list of dictionaries with all the users of the domain.
    """
    out = []
    last_record = False
    first_record = True

    request = None
    response = None
    while not last_record:
        if first_record:
            request = service.users().list(customer='my_customer',
                                            maxResults=max_results,
                                            orderBy=order_by,
                                            sortOrder=sort_order)
            first_record = False
        else:
            request = service.users().list_next(previous_request=request, previous_response=response)
        if request:
            response = request.execute()
            users = response.get('users', [])
            for user in users:
                out.append(parse_user_record(user))
        else:
            last_record = True

    return out


def check_user_exists_admin_id(admin_id, users_list):
    """
    Checks if a person has an account in our directory. If she has account or accounts.
    this function returns a list of them
    :param admin_id: The educational administration code that identifies this person.
    :param users_list: A list of dictionaries as defined by parse_user_records
    :return: A user account's list or an empty list.
    """
    out = []

    for user in users_list:
        if user['EDUCATIVE_ADMINISTRATION_CODE'] == admin_id:
            out.append(user['EMAIL'])
    return out


def check_user_exist_full_name(full_name, users_list):
    """
    Checks if a person has an account in our directory.
    It checks if there's one or more user accounts for the given full_name.
    It returns a list of emails or an empty list if there isn't an account
    :param full_name: The fullname of the person we are looking for.
    :param users_list: The list of users of the directory where we want to
    search. It must be in the format of the output of parse_user_record
    function.
    :return: A list of emails or an empty list.
    """

    out = []
    for user in users_list:
        if user['FULLNAME'] == full_name:
            out.append(user['EMAIL'])
    return out


def get_user_accounts(full_name, admin_id, users_list):
    """
    Checks if a person has an account in our directory by all possible means.
    It tries to find her using her fullname and then her administrative_id.
    If it finds different accounts by different means it returns the list
    that result when merging both outputs
    :param full_name: The fullname of the person we are looking for.
    :param admin_id: The educational administration code that identifies this person.
    :param users_list:  The list of users of the directory where we want to
    search. It must be in the format of the output of parse_user_record
    function.
    :return: A list of emails or an empty list.
    """

    merged_output = []
    search_by_name_list = []
    search_by_admin_id_list = []

    if full_name:
        search_by_name_list = check_user_exist_full_name(full_name, users_list)

    if admin_id:
        search_by_admin_id_list = check_user_exists_admin_id(admin_id, users_list)

    for record in search_by_name_list:
        if record not in merged_output:
            merged_output.append(record)

    for record in search_by_admin_id_list:
        if record not in merged_output:
            merged_output.append(record)

    return merged_output


def max_id(users_list, default_prefix, max_prefix_length=4):
    """
        Looks into the users_list for those accounts wich begins with the default prefix
        for the organization.
        Then looks for the maximum identification number that has been reached while 
        registering accounts and returns it.
        This is useful when you are registering new accounts to set the new prefixes.

        :param users_list:  The list of users of the directory where we want to
        search. It must be in the format of the output of parse_user_record
        :param default_prefix: A string that represents the first characters of every
        email account in the organization.
        :param max_prefix_length: An integer that represent the maximum number of characters
        that comprise the numerical value that appears after the prefix.

        :return: Returns an integer that represent the maximum value that we have reached while
        creating accounts.
    """
    max_id = -1
    for user in users_list:
        if default_prefix in user['EMAIL']:
            id_actual = int(user['EMAIL'].split('@')[0][max_prefix_length - 1:])
            if id_actual > max_id:
                max_id = id_actual
    return max_id


def get_domain_groups_list(service, domain, max_results=500, order_by='email', sort_order='ASCENDING'):
    """
        Gets the domain group list. Parses it and returns a normalized list of groups.

        :param service: Google service reference
        :param domain: Domain name of the owner of the google directory.
        :param max_results: Maximum number of records that we want to retrieve
        :param order_by: Field by we want to sort the output.
        :param sort_order: Order used to sort the group list, can be 'ASCENDING' or 'DESCENDING'

        :return: Returns a list of records in the format returned by the parse_group_record
        function.
    """
    out = []
    last_record = False
    first_record = True
    request = None
    response = None
    
    while not last_record:
        if first_record:
            request = service.groups().list(customer='my_customer', 
                                            maxResults=max_results, 
                                            orderBy=order_by,
                                            domain=domain)
            first_record = False
        else:
            request = service.groups().list_next(previous_request=request, previous_response=response)
        
        if request:
            response = request.execute()
            groups = response.get('groups', [])
            for group in groups:
                out.append(parse_group_record(group))
        else:
            last_record = True    
    
    return out


def get_domain_group(service, group_id, debug=False):
    """
        Retrieves a single group from the google directory service.
        
        :param service: Google service reference
        :param group_id: Alphanumeric identifier of the group we are looking for.
        :param debug: A boolean that determines if we are going to show verbose error messages or not.

        :return: Returns a group dictionary in the format returned by parse_group_record
        function.
    """
    out = None
    try:
        request = service.groups().get(groupKey=group_id)
        response = request.execute()
        out = parse_group_record(response)
    except HttpError:
        if (debug):
            print('Group %s not found' % (group_id))
    return out


def create_group(service, group_name, domain, debug=False):
    """
        Create a new group of users of the domain.

        :param service: Google service reference
        :param group_name: The name that we want for the new group.
        :param domain: Domain name of the owner of the google directory.
        :param debug: A boolean that determines if we are going to show verbose error 
        messages or not.

        :return: Returns a dictionary with the new group data in the format 
        returned by the parse_group_record function or an empty dictionary if there's
        any error.
    """
    out = {}
    try:
        group_email = '%s@%s' % (group_name, domain)
        request = service.groups().insert(body={'name': group_name, 'email': group_email})
        response = request.execute()
        out = parse_group_record(response)
    
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to create %s' % (group_name,))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def get_group_from_group_list(group_list, group):
    """
        If group is into group_list it returns a dictionary with this group, else returns an empty
        dictionary

        :param group_list: A list of the groups available into the directory of the organization
        :param group: The name of the group we are looking for

        :return: A dictionary with the group if it's present into the directory or an empty dictionary
    """
    out = {}

    for group_record in group_list:
        if group_record['NAME'] == group:
            return group_record
    
    return out


def add_user_to_group(service, group_key, user_email, debug=False):
    """
        Adds a user to a group, if the user is added to the group returns True if
        False in any other cases.
        Usually it will return false because the user is already part of the group.

        :param service: Google service reference
        :param group_key: This can be the group ID or the group email address.
        :param user_email: The email of the user that we want to add to the group.
        :param debug: A boolean that determines if we are going to show verbose error 
        messages or not.

        :return: A boolean, True if the user has been added to the group, false if the
        user were already part of the group.
    """
    out = False
    try:
        request = service.members().insert(groupKey=group_key, body={'email': user_email})
        response = request.execute()
        out = True
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to add user %s to group %s' % (user_email, group_key))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out

def remove_user_from_group(service, group_key, user_email, debug=False):
    """
        Removes a user from a group. Returns True if the user is removed. Returns
        False otherwhise. We can't be sure about the process because the API doesn't
        returns any value. So if there isn't an error we consider that the remove process
        has been succesful.

        :param service: Google service reference
        :param group_key: This can be the group ID or the group email address.
        :param user_email: The email of the user that we want to add to the group.
        :param debug: A boolean that determines if we are going to show verbose error 
        messages or not.        
    """
    out = False
    try:
        request = service.members().delete(groupKey=group_key, memberKey=user_email)
        request.execute()
        out = True
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to remove user %s from group %s' % (user_email, group_key))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def create_user_account(service, email, password, given_name, family_name, external_id=None, debug=False):
    """
        Creates a new user account.

        :param service: Google service reference
        :param email: Email address that we want to assign to the new account
        :param password: Password that we want to assign to the new account
        :param given_name: Given name for the new account.
        :param family_name: Family name for the new account.
        :param external_id: This field it's not intended for this use, but here we will
        store the educational administration identifier assigned by educative authorities to
        each student.
        :param debug: A boolean that determines if we are going to show verbose error 
        messages or not.

        :return: A boolean, True if the account has been create, False otherwise.     
    """
    out = False

    try:
        request_body = {
            'name': {
                'familyName': family_name,
                'givenName': given_name,
            },
            'password': password,
            'primaryEmail': email
        }
        if external_id:
            request_body['externalIds'] = [{'type': 'organization', 'value': external_id}]

        request = service.users().insert(body=request_body)
        response = request.execute()
        out = True
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to create user %s' % (email,))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    
    return out


def change_password(service, email, new_password, debug=False):
    """
        Changes an account password and forces the change at next login

        :param service: Google service reference
        :param email: Email address that we want to assign to the new account
        :param new_password: Password that we want to assign to the new account

        :param debug: A boolean that determines if we are going to show verbose error 
        messages or not.

        :return: A boolean, True if the account has been create, False otherwise. 
    """
    out = False
    try:
        request_body = {
            'password': new_password,
            'changePasswordAtNextLogin': True
        }

        request = service.users().patch(userKey=email, body=request_body)
        response = request.execute()
        print('The response')
        print(response)
        out = True
    except HttpError as e:
        out = False
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to create user %s' % (email,))
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    return out


def main():
    """
    Shows basic usage of the Admin SDK Directory API.
    Prints the emails and names of the first 10 users in the domain.
    """
    creds = None
    # The file directory-token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('../../credentials/directory-token.pickle'):
        with open('../../credentials/directory-token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '../../credentials/directory-credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('../../credentials/directory-token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('admin', 'directory_v1', credentials=creds)

    # Call the Admin SDK Directory API
    print('Getting the users in the domain')

    users = get_users(service)

    #print('Total domain users: %d' % (len(users)))

    groups = get_domain_groups_list(service, 'salesianos-lacuesta.net')
    for g in groups:
        print('\n\n')
        print(g)

    #group = get_domain_group(service, '02fk6b3p4c17l4v')
    #print(group)

    # create_group(service, 'rantanplan', 'salesianos-lacuesta.net', True)

    # add_user_to_group(service, '02fk6b3p4c17l4v', 'goyoregalado@salesianos-lacuesta.net', True)

    # create_user_account(service, 'rantanplanuser@salesianos-lacuesta.net', 'luckyluke', 'rantan', 'plan', None, True)

    # create_user_account(service, 'rantanplanuser2@salesianos-lacuesta.net', 'luckyluke', 'rantan', 'plan', 'ASLKDJFLSDKJFLSDKFJ', True)

    # create_user_account(service, 'rantanplanuser3@salesianos-lacuesta.net', 'luckyluke', 'rantan', 'plan', 'ASLKDJFLSDKJFLSDKFJ', True)

    # create_user_account(service, 'rantanplanuser4@salesianos-lacuesta.net', 'luckyluke', 'rantan', 'plan', 'ASLKDJFLSDKJFLSDKFJ', True)

    # remove_user_from_group(service, '02fk6b3p4c17l4v', 'goyoregalado@salesianos-lacuesta.net')
    create_user_account(service, 'rantanplanuser8@salesianos-lacuesta.net', 'luckyluke', 'rantan', 'plan', 'ASLKDJFLSDKJFLSDKFJ', True)
    change_password(service, 'rantanplanuser8@salesianos-lacuesta.net', 'alsidhf12.', True)

if __name__ == '__main__':
    main()
