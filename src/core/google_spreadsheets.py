import pickle
import os.path
import simplejson

from googleapiclient.discovery import build

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.errors import HttpError

SCOPES = [
    'https://www.googleapis.com/auth/spreadsheets.readonly',
    'https://www.googleapis.com/auth/spreadsheets'
]

TEACHER_SPREADSHEET_ID = '1HeRhIK5538V7c7fARnnfHMBU6D5_Y1AiKe7N8iV8M0E'
TEACHER_RANGE_NAME = 'Hoja 1!A1:G400'

STUDENT_SPREADSHEET_ID = '17z8wgkw27__n30uIAjrUQ0T4QdsxBVs8NDWS-5PAjkQ'
STUDENT_RANGE_NAME = 'Hoja 1!A1:J2000'


def get_spreadsheet_data(service, spreadsheet_id, cells_range):
    """
    Access a google Drive's spreadsheet that contains the relationship between
    teachers and subjects.
    :param service: Google service reference.
    :param spreadsheet_id: Unique identifier of the spreadsheet in Google Drive unit.
    :param cells_range: Range of cells from which we need to retrieve the values in A1 format.
    :return: Return a list of lists with the records of the file.
    """

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=spreadsheet_id, range=cells_range).execute()
    values = result.get('values', [])
    return values


def write_students_data(service, spreadsheet_id, cells_range, students_dict, debug=False):
    """
    Access a google Drive's spreadsheet and writes into it the data relative to the students
    accounts.

    :param service: Google service reference.
    :param spreadsheet_id: Unique identifier of the spreadsheet in Google Drive unit.
    :param cells_range: Range of cells from which we need to retrieve the values in A1 format.
    :param students_dict: A dictionary indexed by email as the output of 
    students.student_enrolment.bulk_student_user_creation
    :return: Return a list of lists with the records of the file.
    """
    out = False
    values = []
    emails_list = students_dict.keys()
    for email in emails_list:

        student = students_dict[email]
        
        row = [
            student['STUDENT_FIRSTNAME'],
            student['STUDENT_MIDDLENAME'],
            student['STUDENT_LASTNAME'],
            student['CLASS_GROUP'],
            student['STUDENT_EMAIL'],
            student['STUDENT_PASSWORD']
        ]
        values.append(row)

    try: 
        sheet = service.spreadsheets()
        # valueInputOption arg represents two use cases:
        # RAW: The formulaes aren't interpreted it will write =1+2 in the cell
        # USER_ENTERED: Will act as if the user wrote into the cell so, it will write 3 to the cell
        result = sheet.values().append(
                    spreadsheetId=spreadsheet_id, 
                    range=cells_range,
                    valueInputOption='USER_ENTERED',
                    body={
                        'range': cells_range,
                        'values': values,
                        'majorDimension': 'ROWS'
                    }
                ).execute()
        out = True
    except HttpError as e:
        if debug:
            error = simplejson.loads(e.content).get('error')
            print('Error: Trying to write students account to spreadsheet')
            print('\t => Error code: %d, Error message: %s' % (error['code'], error['message']))
    
    return out    


def main():
    creds = None
    # The file spreadsheet-token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('../../credentials/spreadsheet-token.pickle'):
        with open('../../credentials/spreadsheet-token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '../../credentials/spreadsheet-credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('../../credentials/spreadsheet-token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    values = get_spreadsheet_data(service, TEACHER_SPREADSHEET_ID, TEACHER_RANGE_NAME)

    if not values:
        print('No data found.')
    else:
        print('Printing teacher data')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            print(row)

    values = get_spreadsheet_data(service, STUDENT_SPREADSHEET_ID, STUDENT_RANGE_NAME)

    if not values:
        print('No data found.')
    else:
        print('Printing students data')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            print(row)


if __name__ == '__main__':
    main()

