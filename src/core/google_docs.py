import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/documents']

# The ID of a sample document.
DOCUMENT_ID = '163yNVbfqP4IRiLNaQ6fljGfDNQ1UMSZPU_MLrAVh7z8'

def get_document_content(service, document_id):
  """
    Gets a document from a drive account using its document_id and returns
    a string with its first paragraph
    :param service: Google service reference
    :param document_id: Unique identifier of the document in Google Drive unit.
  """
  output = ''
  document = service.documents().get(documentId=document_id).execute()

  document_contents = document.get('body').get('content')

  for doc_cont in document_contents:
    if 'paragraph' in doc_cont.keys():
      parrafo = doc_cont['paragraph']
      elements = parrafo['elements']
      for el in elements:
        if 'textRun' in el.keys():
          output = el['textRun']['content']
  return output

def write_into_document(service, document_id, text):
  """
    Write the text passed as an argument to the document represented by document_id

    :param service: Google service reference.
    :param document_id: Unique identifier of the document in Google Drive unit.
    :param text: The text that we want to put at the beginning of the file.
  """
  requests = [
    {
      'insertText': {
        'location': {
          'index': 1,
        },
        'text': text
      }
    }
  ]

  result = service.documents().batchUpdate(
      documentId=DOCUMENT_ID, 
      body={'requests': requests}).execute()
  return result

def estimate_document_length(service, document_id):
  """
    Estimates the maximum length of a document stored into a Drive unit, this is a naive
    estimation, because we consider that the document has an unique paragraph.
    :param service: Google service reference
    :param document_id: Unique identifier of the document in Google Drive unit.

    :return: Returns the length of the paragraph or -1 if there isn't any paragraph at all.
  """
  output = -1
  document = service.documents().get(documentId=document_id).execute()

  document_contents = document.get('body').get('content')

  for doc_cont in document_contents:
    if 'paragraph' in doc_cont.keys():
      paragraph = doc_cont['paragraph']
      output = paragraph['elements'][0]['endIndex'] - 1 

  return output

def delete_document_content(service, document_id, lastIndex):
  """
    Delete the content of the drive document identified by document_id.
    :param service: Google service reference
    :param document_id: Unique identifier of the document in Google Drive unit.
  """
  requests = [
      {
          'deleteContentRange': {
              'range': {
                  'startIndex': 1,
                  'endIndex': lastIndex,
              }

          }

      },
  ]
  result = service.documents().batchUpdate(
      documentId=document_id, 
      body={'requests': requests}).execute()
  return result
  

def main():
  """
    Shows basic usage of the Docs API.
    Prints the title of a sample document.
  """
  creds = None
  # The file token.pickle stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  if os.path.exists('../../credentials/docs-token.pickle'):
      with open('../../credentials/docs-token.pickle', 'rb') as token:
          creds = pickle.load(token)
  # If there are no (valid) credentials available, let the user log in.
  if not creds or not creds.valid:
      if creds and creds.expired and creds.refresh_token:
          creds.refresh(Request())
      else:
          flow = InstalledAppFlow.from_client_secrets_file(
              '../../credentials/docs-credentials.json', SCOPES)
          creds = flow.run_local_server()
      # Save the credentials for the next run
      with open('../../credentials/docs-token.pickle', 'wb') as token:
          pickle.dump(creds, token)

  service = build('docs', 'v1', credentials=creds)

  # Retrieve the documents contents from the Docs service.
  text = get_document_content(service, DOCUMENT_ID)
  print('The text: %s' % (text))
  lastIndex = estimate_document_length(service, DOCUMENT_ID)
  delete_document_content(service, DOCUMENT_ID, lastIndex)
  write_into_document(service, DOCUMENT_ID, 'meneeeño')

if __name__ == '__main__':
    main()