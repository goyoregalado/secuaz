import pickle
import os.path
import hashlib
import time
import codecs

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


from prettyconf import Configuration
from prettyconf.loaders import EnvFile

from core.auth import check_auth
from core.core import student_subject_processing, generate_configuration_dictionary, normalize_classgroup_name, \
                        teacher_subject_processing
from core.google_spreadsheets import get_spreadsheet_data, write_students_data
from core.google_docs import get_document_content, write_into_document, estimate_document_length, delete_document_content
from core.google_directory import get_users, max_id, get_domain_groups_list

from students.student_enrolment import bulk_student_user_creation, bulk_student_enrolment
from teachers.teacher_enrolment import bulk_teacher_enrolment

from core.syncronization import get_modified_records, generate_hash_table, process_modified_records


SCOPES = [
    'https://www.googleapis.com/auth/spreadsheets',
    'https://www.googleapis.com/auth/documents',
    'https://www.googleapis.com/auth/admin.directory.user',
    'https://www.googleapis.com/auth/admin.directory.group',
    'https://www.googleapis.com/auth/admin.directory.group.member',
    'https://www.googleapis.com/auth/classroom.courses',
    'https://www.googleapis.com/auth/classroom.profile.emails',
    'https://www.googleapis.com/auth/classroom.rosters',
    'https://www.googleapis.com/auth/classroom.profile.photos'
]


hash_table = {}


def main():
    app_path = os.path.dirname(__file__)

    root_path = os.path.realpath(os.path.join(app_path, '..'))
    project_config_path = os.path.join(os.path.join(root_path, 'config'))
    project_config_file = os.path.join(project_config_path, 'secuaz19-20.env')

    project_credentials_path = os.path.join(root_path, 'credentials')
    project_credentials_file = os.path.abspath(os.path.join(project_credentials_path, 'docs-credentials.json'))
    project_token_file = os.path.abspath(os.path.join(project_credentials_path,'project-global-token.pickle'))

    project_cache_path = os.path.join(root_path, 'cache')
    project_cache_file = os.path.abspath(os.path.join(project_cache_path, 'data.hash'))

    project_hash_table_file = os.path.abspath(os.path.join(project_cache_path, 'hash_table'))



    config_file_name = os.path.abspath(project_config_file)
    env_loader =[EnvFile(filename=config_file_name, var_format=str.upper)]
    config = Configuration(loaders=env_loader)

    COURSE_STRING = config('COURSE')
    DOMAIN_NAME = config('DOMAIN_NAME')

    EMAIL_PREFIX = config('EMAIL_PREFIX')

    STUDENT_DRIVE_FILE_ID = config('STUDENT_DRIVE_FILE_ID')
    STUDENT_RANGE_NAME = config('STUDENT_RANGE_NAME')

    STUDENT_OUTPUT_FILE_ID = config('STUDENT_OUTPUT_FILE_ID')
    STUDENT_OUTPUT_RANGE_NAME = config('STUDENT_OUTPUT_RANGE_NAME')

    TEACHER_DRIVE_FILE_ID = config('TEACHER_DRIVE_FILE_ID')
    TEACHER_RANGE_NAME = config('TEACHER_RANGE_NAME')

    CLASSROOM_GROUP_ALIAS = config('CLASSROOM_GROUP_ALIAS')

    EDUCATIONAL_LEVELS = config('EDUCATIONAL_LEVELS', cast=config.list)
    EDUCATIONAL_LEVELS_CODES = config('EDUCATIONAL_LEVELS_CODES', cast=config.list)

    CLASSROOM_PREFIXES = config('CLASSROOM_PREFIXES', cast=config.list)
    CLASSROOM_CODES = config('CLASSROOM_CODES', cast=config.list)

    VALID_SECTIONS = config('VALID_SECTIONS', cast=config.list)
    SECTIONS_CODES = config('SECTIONS_CODES', cast=config.list)



    classroom_prefixes_dictionary = generate_configuration_dictionary(CLASSROOM_PREFIXES, CLASSROOM_CODES)
    sections_dictionary = generate_configuration_dictionary(VALID_SECTIONS, SECTIONS_CODES)

    sheets_service = check_auth('sheets', 'v4', project_token_file, project_credentials_file, SCOPES)

    docs_service = check_auth('docs', 'v1', project_token_file, project_credentials_file, SCOPES)

    directory_service = check_auth('admin', 'directory_v1', project_token_file, project_credentials_file, SCOPES)

    classroom_service = check_auth('classroom', 'v1', project_token_file, project_credentials_file, SCOPES)


    teachers_actual_data = get_spreadsheet_data(sheets_service, TEACHER_DRIVE_FILE_ID, TEACHER_RANGE_NAME)
    teachers_actual_data = teacher_subject_processing(teachers_actual_data, 
                                    classroom_prefixes_dictionary, 
                                    sections_dictionary, 
                                    COURSE_STRING)

    teachers_failed = bulk_teacher_enrolment(directory_service, 
                            classroom_service, 
                            CLASSROOM_GROUP_ALIAS, 
                            teachers_actual_data,
                            debug=True,
                            dry_run=False)

    print('These teachers failed when trying to enroll')
    for teacher in teachers_failed:
        print(teacher)


    actual_data = get_spreadsheet_data(sheets_service, STUDENT_DRIVE_FILE_ID, STUDENT_RANGE_NAME)
    actual_data = student_subject_processing(actual_data, 
                                    classroom_prefixes_dictionary, 
                                    sections_dictionary, 
                                    COURSE_STRING)

    actual_data_hash_value = hashlib.sha512(str(actual_data).encode('utf-8')).hexdigest()

    hash_value = None

    if os.path.exists(project_cache_file):
        with open(project_cache_file, 'r') as hash_file:
            hash_value = hash_file.readline().strip()

    if not hash_value:
        # This is the first time that syncronization runs so we must generate the data hash table.
        # We consider that every single record is modified.

        print('First syncronization, generating hash table')
        hash_table = generate_hash_table(actual_data)

        with open(project_cache_file, 'w') as hash_file:
            hash_file.write(actual_data_hash_value)

        with open(project_hash_table_file, 'wb') as hash_table_file:
            pickle.dump(hash_table, hash_table_file)

        print('Considering all records as modified')
        print('Total number of records: ', len(actual_data))

        print('Retrieving all system accounts')
        directory_user_list = get_users(directory_service)
        print('Total number of system accounts: ', len(directory_user_list))

        print('Retrieving all system groups')
        directory_group_list = get_domain_groups_list(directory_service, DOMAIN_NAME)
        print('Total number of system groups: ', len(directory_group_list))

        maximum_userID = max_id(directory_user_list, EMAIL_PREFIX)

        student_dict = bulk_student_user_creation(
                            directory_service, 
                            actual_data, 
                            directory_user_list, 
                            EMAIL_PREFIX, 
                            maximum_userID + 1, 
                            DOMAIN_NAME, 
                            directory_group_list, 
                            debug=True, 
                            dry_run=False
                        )

        status = write_students_data(sheets_service, STUDENT_OUTPUT_FILE_ID, STUDENT_OUTPUT_RANGE_NAME, student_dict, debug=True)
        if status:
            print('Students added to output file')
        else:
            print('Error while adding students to output file')
        
        students_with_enrollment_problems = bulk_student_enrolment(classroom_service, student_dict, debug=True, dry_run=False)

        print('Students with problems in enrollment')
        for student in students_with_enrollment_problems:
            print(student)
        print('Total number of students with problems in enrollment: ', len(students_with_enrollment_problems))

    else:
        if hash_value != actual_data_hash_value:
            with open(project_hash_table_file, 'rb') as hash_table_file:
                hash_table = pickle.load(hash_table_file)

            # There's a modification in the data, so we must check
            print('Changes detected')
            modified_records = get_modified_records(hash_table, actual_data)
            print('Modified records')

            print(modified_records)

            processed_modified_records = process_modified_records(modified_records['modified'])

            # FIXME: New hashes contains modified key and not present_key
            # This should process modified registers and after that it should
            # generate the new hashes.

            new_hashes = generate_hash_table(processed_modified_records)

            

            for key in new_hashes.keys():
                hash_table[key] = new_hashes[key]

            with open(project_cache_file, 'w') as hash_file:
                hash_file.write(actual_data_hash_value)

            # Store hash table in the file.
            with open(project_hash_table_file, 'wb') as hash_table_file:
                pickle.dump(hash_table, hash_table_file)
        else:
            print('No changes detected')

if __name__ == '__main__':
    while(1):
        main()
        time.sleep(3600)
